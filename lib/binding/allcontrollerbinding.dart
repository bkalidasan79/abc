import 'package:get/get.dart';
import 'package:newpanale/controllers/profile_controller.dart';
import 'package:newpanale/controllers/users_controller.dart';

class AllControllerBinding implements Bindings {
  @override
  Future<void> dependencies() async {
    Get.lazyPut<UsersController>(() => UsersController());
    Get.lazyPut<ProfileController>(() => ProfileController());
  }
}
