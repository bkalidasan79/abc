import 'package:get/get.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../routing/routes.dart';

class DriversController extends GetxController {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  late String token;

  @override
  Future<void> onInit() async {
    // jwtController.checkAuth();
    super.onInit();
    final SharedPreferences prefs = await _prefs;
    token = prefs.getString('access_token')!;
    if (token.isEmpty) Get.offAllNamed(authenticationPageRoute);
    if (JwtDecoder.isExpired(token)) Get.offAllNamed(authenticationPageRoute);
  }

  @override
  void onReady() {
    // Get called after widget is rendered on the screen
    super.onReady();
  }

  @override
  void onClose() {
    //Get called when controller is removed from memory
    super.onClose();
  }
}
