// ignore_for_file: prefer_const_constructors

import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class ImageController extends GetxController {
  Rx<Uint8List> webimage = Uint8List(8).obs;
  Rx<bool> imagee = false.obs;
  var selectedfile = ''.obs;

  Future<void> pickImage(ImageSource imageSource) async {
    final ImagePicker picker = ImagePicker();
    XFile? image = await picker.pickImage(source: imageSource);
    if (image == null) {
      imagee.value = false;
      Get.snackbar('No Image - Android', 'Image can\'t Select');
    } else {
      if (!kIsWeb) {
        selectedfile.value = image.path;
        imagee.value = true;
      } else if (kIsWeb) {
        var f = await image.readAsBytes();
        webimage.value = f;
        selectedfile.value = '';
        imagee.value = true;
      }
    }
  }
}
