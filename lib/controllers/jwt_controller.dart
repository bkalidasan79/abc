import 'package:get/get.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:newpanale/routing/routes.dart';

class MyJwtController extends GetxController {
  checTokenkExpired(String token) {
    if (token.isEmpty) Get.offAllNamed(authenticationPageRoute);
    if (JwtDecoder.isExpired(token)) Get.offAllNamed(authenticationPageRoute);

    return true;
  }

  getTokenDetail(String token, String id) {
    Map<String, dynamic>? decodedToken = JwtDecoder.tryDecode(token);
    return decodedToken![id];
  }
}
