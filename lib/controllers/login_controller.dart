// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:newpanale/models/profile_mode.dart';
import 'package:newpanale/routing/routes.dart';
import 'package:newpanale/services/auth_service.dart';
import 'package:newpanale/utils/api_endpoints.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:newpanale/models/login_mode.dart';

class LoginController extends GetxController {
  TextEditingController emailController =
      TextEditingController(text: 'john@mail.com');
  TextEditingController passwordController =
      TextEditingController(text: 'changeme');
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  var login = <LoginModel>[].obs;
  Map<String, dynamic> errordata = {};
  var profileModel = <ProfileModel>[].obs;

  Future<void> loginWithEmail() async {
    var headers = {'Content-Type': 'application/json'};
    try {
      var url = Uri.parse(
          ApiEndPoints.baseUrl + ApiEndPoints.authEndpoints.loginEmail);

      final logindata = LoginModel(
        email: emailController.text.trim(),
        password: passwordController.text,
      );

      http.Response response = await http.post(url,
          body: jsonEncode(logindata.toJson()), headers: headers);
      //rprint(response.statusCode);

      if (response.statusCode == 201) {
        final json1 = jsonDecode(response.body);
        var token = json1['access_token'];
        final SharedPreferences prefs = await _prefs;
        await prefs.setString('access_token', token);

        emailController.clear();
        passwordController.clear();
        AuthService.to.login();
        AuthService.to.setusername('kja');

        Get.offAllNamed(overViewPageRoute);
      } else if (response.statusCode == 400 || response.statusCode == 401) {
        errordata = jsonDecode(response.body);
        Get.snackbar('Error', errordata['message']);
        Get.offAllNamed(authenticationPageRoute);
      }
    } catch (error) {
      Get.snackbar('Error', error.toString());
    }
  }

  Future<void> logoutwithemail() async {
    final SharedPreferences prefs = await _prefs;
    var token = prefs.getString('access_token');
    var headers = {
      'Content-Type': 'application/json',
      "Authorization": "Bearer $token"
    };
    try {
      var url = Uri.parse(
          ApiEndPoints.baseUrl + ApiEndPoints.authEndpoints.logoutEmail);
      http.Response response = await http.post(url, headers: headers);
      errordata = jsonDecode(response.body);

      if (response.statusCode == 200) {
        prefs.remove('access_token');
        AuthService.to.logout();
        Get.snackbar('Success', errordata['message']);
        Get.offAllNamed(authenticationPageRoute);
      } else if (response.statusCode == 400 || response.statusCode == 401) {
        Get.snackbar('Error', errordata['message']);
      }
    } catch (error) {
      Get.snackbar('Error', error.toString());
    }
  }
}
