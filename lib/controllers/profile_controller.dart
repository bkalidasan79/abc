// ignore_for_file: prefer_const_constructors, await_only_futures

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:newpanale/controllers/image_controller.dart';
import 'package:newpanale/controllers/jwt_controller.dart';
import 'package:newpanale/services/auth_service.dart';
import 'package:newpanale/utils/api_endpoints.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:newpanale/models/profile_mode.dart';

class ProfileController extends GetxController {
  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController idController = TextEditingController();
  TextEditingController imageurlController = TextEditingController();
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  MyJwtController jwtController = MyJwtController();

  final imageController = Get.put(ImageController());
  final imagePage = ''.obs;
  final imageurl = ''.obs;
  List<int>? imagefile;
  Map<String, dynamic> errordata = {};
  var profileModel = <ProfileModel>[].obs;

  @override
  void onInit() {
    super.onInit();
    getProfile();
  }

  Future<void> saveProfile() async {
    final SharedPreferences prefs = await _prefs;
    var token = prefs.getString('access_token');

    try {
      if (jwtController.checTokenkExpired(token!)) {
        var url = Uri.parse(
            ApiEndPoints.baseUrl + ApiEndPoints.profileEndpoind.profileUpdate);

        http.MultipartRequest request = http.MultipartRequest('POST', url);
        request.headers.addAll(<String, String>{
          'Authorization': 'Bearer $token',
          "Content-Type": "multipart/form-data"
        });

        if (imageController.imagee.value) {
          if (kIsWeb) {
            imagefile = imageController.webimage.value;
            request.files.add(await http.MultipartFile.fromBytes(
                "imageurl", imagefile!,
                filename: 'test'));
          } else {
            request.files.add(await http.MultipartFile.fromPath(
              "imageurl",
              imageController.selectedfile.value,
              filename: 'test.jpg',
            ));
          }
        }

        Map<String, String> fields = {};
        fields.addAll(<String, String>{
          'email': emailController.text.trim(),
          'name': nameController.text.trim(),
          'id': idController.text,
        });
        request.fields.addAll(fields);
        http.StreamedResponse response = await request.send();

        Map map = jsonDecode(await response.stream.bytesToString());

        // print(map);
        if (response.statusCode == 200) {
          AuthService.to.setusername(nameController.text);

          Get.snackbar('Profile', map["message"]);
        } else if (response.statusCode == 400 || response.statusCode == 401) {
          Get.snackbar('Error Profile: ', map["error"]);
        }
      }
    } catch (error) {
      Get.snackbar('Error Profile', error.toString());
    }
  }

  Future<void> getProfile() async {
    final SharedPreferences prefs = await _prefs;
    String? token = prefs.getString('access_token');
    var useremail = '';
    var headers = {
      'Content-Type': 'application/json',
      "Authorization": "Bearer $token"
    };
    try {
      if (jwtController.checTokenkExpired(token!)) {
        useremail = jwtController.getTokenDetail(token, 'email');
      }

      final profileModel = ProfileModel(
        email: useremail,
        name: '',
      );

      var url = Uri.parse(
          ApiEndPoints.baseUrl + ApiEndPoints.profileEndpoind.getProfile);

      http.Response response = await http.post(url,
          body: jsonEncode(profileModel.toJson()), headers: headers);

      if (response.statusCode == 200) {
        final json = jsonDecode(response.body);
        //  print(json['user']);
        emailController.text = json['user']['email'];
        nameController.text = json['user']['name'];
        idController.text = json['user']['id'].toString();
        imageurl.value = json['user']['imageurl'];
        // print(imageurl.value);
      } else if (response.statusCode == 400 || response.statusCode == 401) {
        errordata = jsonDecode(response.body);
        Get.snackbar('getProfile', errordata['message']);
      }
    } catch (error) {
      Get.snackbar('getProfile-e', error.toString());
    }
  }

  init() {}
}
