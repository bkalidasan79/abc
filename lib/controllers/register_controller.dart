// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:newpanale/routing/routes.dart';
import 'package:newpanale/utils/api_endpoints.dart';
import 'dart:convert';
import 'package:newpanale/models/register_model.dart';

class RegisterController extends GetxController {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordconfirmationController =
      TextEditingController();
  var register = <RegisterModel>[].obs;
  Map<String, dynamic> errordata = {};

  Future<void> registerWithEmail() async {
    var headers = {'Content-Type': 'application/json'};
    try {
      var url = Uri.parse(
          ApiEndPoints.baseUrl + ApiEndPoints.authEndpoints.registerEmail);

      final registerdata = RegisterModel(
          email: emailController.text.trim(),
          name: nameController.text,
          password: passwordController.text,
          passwordConfirmation: passwordconfirmationController.text);

      http.Response response = await http.post(url,
          body: jsonEncode(registerdata.toJson()), headers: headers);

      if (response.statusCode == 200) {
        Get.snackbar('Success', 'user has been successfully created');
        emailController.clear();
        passwordController.clear();
        nameController.clear();
        passwordconfirmationController.clear();

        Get.offAllNamed(authenticationPageRoute);
      } else if (response.statusCode == 400) {
        errordata = jsonDecode(response.body);
        Get.snackbar('Error', errordata['error']);
      }
    } catch (error) {
      Get.snackbar('Error', error.toString());
    }
  }
}
