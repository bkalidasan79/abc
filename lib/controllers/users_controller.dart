import 'dart:async';
import 'dart:convert';
import 'package:get/state_manager.dart';
import 'package:get/get.dart';
import 'package:newpanale/controllers/jwt_controller.dart';
import 'package:newpanale/utils/api_endpoints.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:newpanale/models/users_mode.dart';
import 'package:http/http.dart' as http;

class UsersController extends GetxController {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  MyJwtController jwtController = MyJwtController();

/*   var usersList = <UsersModel>[].obs;
  var isLoading = true.obs;

  @override
  void onInit() {
    fetchUsers();
    super.onInit();
  }

  void fetchUsers() async {
    try {
      final SharedPreferences prefs = await _prefs;
      String? token = prefs.getString('access_token');
      isLoading(true);
      var users = await UserServices.fetchUsers(token!);
      if (users != null) {
        usersList = users;
      }
    } finally {
      isLoading(false);
    }
  } */

  Map<String, dynamic> errordata = {};
  RxList users = [].obs;

  RxList myUsersModel = [].obs;
  var isLoading = true.obs;

  @override
  void onReady() async {
    // Get called after widget is rendered on the screen
    await getUsers();
  }

  getUsers() async {
    final SharedPreferences prefs = await _prefs;
    String? token = prefs.getString('access_token');

    var headers = {
      'Content-Type': 'application/json',
      "Authorization": "Bearer $token"
    };

    try {
      if (jwtController.checTokenkExpired(token!)) {}

      var url = Uri.parse(
          "${ApiEndPoints.baseUrl}${ApiEndPoints.usersEndpoind.allUsers}");
      http.Response response = await http.get(url, headers: headers);
      const Duration(seconds: 2);
      if (response.statusCode == 200) {
        final json = await jsonDecode(response.body);
        myUsersModel = [].obs;

        for (var u in json) {
          UsersModel users = UsersModel(
              email: u['email'], id: u['id'], name: u['name'], status: 0);
          //  status: u['status']);
          myUsersModel.add(users);
        }
        // print(myUsersModel);
        isLoading(false);
      } else {
        errordata = jsonDecode(response.body);
        isLoading(true);
      }
    } catch (error) {
      isLoading(true);
    }
  }
}
