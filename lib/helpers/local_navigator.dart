import 'package:flutter/cupertino.dart';
import 'package:newpanale/routing/router.dart';
import 'package:newpanale/routing/routes.dart';

import 'package:newpanale/constants/controllers.dart';

Navigator localNavigator() => Navigator(
      key: navigationController.navigatorKey,
      initialRoute: overViewPageRoute,
      onGenerateRoute: generateRoute,
    );
