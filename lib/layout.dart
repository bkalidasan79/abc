// ignore_for_file: prefer_const_constructors
import 'package:newpanale/helpers/local_navigator.dart';
import 'package:newpanale/widgets/side_menu.dart';
import 'package:flutter/material.dart';
import 'package:newpanale/helpers/reponsiveness.dart';
import 'package:newpanale/widgets/large_screen.dart';
import 'package:newpanale/widgets/top_nav.dart';

class SiteLayout extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  SiteLayout({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      extendBodyBehindAppBar: true,
      appBar: topNavigationBar(context, scaffoldKey),
      drawer: Drawer(
        child: myDrawer,
      ),
      body: ResponsiveWidget(
          largeScreen: LargeScreen(),
          smallScreen: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: localNavigator(),
          )),
    );
  }
}
