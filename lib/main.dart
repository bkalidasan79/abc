// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:newpanale/binding/allcontrollerbinding.dart';
import 'package:newpanale/constants/style.dart';
import 'package:newpanale/controllers/menu_controller.dart';
import 'package:newpanale/controllers/navigation_controller.dart';
import 'package:newpanale/layout.dart';
import 'package:newpanale/middleware/auth_middleware.dart';
import 'package:newpanale/pages/404/error.dart';
import 'package:newpanale/pages/authentication/authentication.dart';
import 'package:newpanale/pages/authentication/forgot_password.dart';
import 'package:newpanale/pages/authentication/register.dart';
import 'package:newpanale/routing/routes.dart';
import 'package:newpanale/services/auth_service.dart';

void main() {
  Get.put(CustomMenuController());
  Get.put(NavigationController());
  Get.put(AuthService());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: authenticationPageRoute,
      initialBinding: AllControllerBinding(),
      unknownRoute: GetPage(
          name: '/not-found',
          page: () => PageNotFound(),
          transition: Transition.fadeIn),
      getPages: [
        GetPage(
            name: rootRoute,
            middlewares: [EnsureAuthMiddleware()],
            page: () {
              return SiteLayout();
            }),
        GetPage(
            name: authenticationPageRoute, page: () => AuthenticationPage()),
        GetPage(name: registerPageRoute, page: () => RegisterPage()),
        GetPage(
            name: forgotpasswordPageRoute,
            page: () => const ForgotpasswordPage()),
      ],
      debugShowCheckedModeBanner: false,
      title: 'Dashboard',
      theme: ThemeData(
        scaffoldBackgroundColor: light,
        textTheme: GoogleFonts.mulishTextTheme(Theme.of(context).textTheme)
            .apply(bodyColor: Colors.black),
        pageTransitionsTheme: PageTransitionsTheme(builders: {
          TargetPlatform.iOS: FadeUpwardsPageTransitionsBuilder(),
          TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
        }),
        primarySwatch: Colors.blue,
      ),
    );
  }
}
