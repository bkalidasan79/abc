import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:newpanale/routing/routes.dart';
import 'package:newpanale/services/auth_service.dart';

class EnsureAuthMiddleware extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    //print(route.toString());
    if (!AuthService.to.isLoggedInValue) {
      return const RouteSettings(name: authenticationPageRoute);
    }

    return null;
  }
}
