class ProfileModel {
  ProfileModel({
    required this.email,
    required this.name,
    this.id,
    this.image,
  });
  late final String email;
  late final String name;
  late final int? id;
  late final String? image;

  ProfileModel.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    name = json['name'];
    id = json['id'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['email'] = email;
    data['name'] = name;
    data['id'] = id;
    data['image'] = image;
    return data;
  }
}
