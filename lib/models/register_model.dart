class RegisterModel {
  RegisterModel({
    required this.email,
    required this.name,
    required this.password,
    required this.passwordConfirmation,
  });
  late final String email;
  late final String name;
  late final String password;
  late final String passwordConfirmation;

  RegisterModel.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    name = json['name'];
    password = json['password'];
    passwordConfirmation = json['password_confirmation'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['email'] = email;
    data['name'] = name;
    data['password'] = password;
    data['password_confirmation'] = passwordConfirmation;
    return data;
  }
}
