// To parse this JSON data, do
//
//     final usersmodel = usersmodelFromJson(jsonString);

import 'dart:convert';

UsersModel usersmodelFromJson(String str) =>
    UsersModel.fromJson(json.decode(str));

String usersmodelToJson(UsersModel data) => json.encode(data.toJson());

class UsersModel {
  final int id;
  final String name;
  final String email;
  final int status;
  final String? imageurl;

  UsersModel({
    required this.id,
    required this.name,
    required this.email,
    required this.status,
    this.imageurl,
  });

  factory UsersModel.fromJson(Map<String, dynamic> json) => UsersModel(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        status: json["status"],
        imageurl: json["imageurl"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "status": status,
        "imageurl": imageurl,
      };
}
