// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:form_validator/form_validator.dart';
import 'package:newpanale/constants/style.dart';
import 'package:newpanale/controllers/login_controller.dart';
import 'package:newpanale/routing/routes.dart';
import 'package:newpanale/widgets/custom_text.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/gestures.dart';

// ignore: must_be_immutable
class AuthenticationPage extends StatelessWidget {
  AuthenticationPage({super.key});
  GlobalKey<FormState> authKey = GlobalKey<FormState>();
  LoginController loginController = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          constraints: BoxConstraints(maxWidth: 400),
          padding: EdgeInsets.all(24),
          child: Form(
            key: authKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 12),
                      child: Image.asset("assets/icons/logo.png"),
                    ),
                    Expanded(child: Container()),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    Text("Login",
                        style: GoogleFonts.roboto(
                            fontSize: 30, fontWeight: FontWeight.bold)),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    CustomText(
                      text: "Welcome back to the admin panel.",
                      color: lightGrey,
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                TextFormField(
                  controller: loginController.emailController,
                  validator: ValidationBuilder().email().maxLength(50).build(),
                  enabled: false,
                  decoration: InputDecoration(
                      labelText: "Email",
                      hintText: "abc@domain.com",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20))),
                ),
                SizedBox(
                  height: 15,
                ),
                TextFormField(
                  obscureText: true,
                  controller: loginController.passwordController,
                  validator: ValidationBuilder()
                      .regExp(RegExp(r"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)"),
                          "Capital, small letter & Number & Special")
                      .minLength(6)
                      .maxLength(8)
                      .build(),
                  decoration: InputDecoration(
                      labelText: "Password",
                      hintText: "123",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20))),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Checkbox(value: true, onChanged: (value) {}),
                        CustomText(
                          text: "Remeber Me",
                        ),
                      ],
                    ),
                    RichText(
                        text: TextSpan(children: [
                      TextSpan(
                          text: "Forgot password..? ",
                          style: TextStyle(color: active),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Get.offAllNamed(forgotpasswordPageRoute);
                            })
                    ]))
                    //  CustomText(text: "Forgot password?", color: active)
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                InkWell(
                  onTap: () async {
                    // Get.offAllNamed(rootRoute);
                    if (authKey.currentState!.validate()) {
                      loginController.loginWithEmail();
                    }
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: active, borderRadius: BorderRadius.circular(20)),
                    alignment: Alignment.center,
                    width: double.maxFinite,
                    padding: EdgeInsets.symmetric(vertical: 16),
                    child: CustomText(
                      text: "Login",
                      color: Colors.white,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                RichText(
                    text: TextSpan(children: [
                  TextSpan(text: "i don't have an account create.. "),
                  TextSpan(
                      text: "Sign up! ",
                      style: TextStyle(color: active),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Get.offAllNamed(registerPageRoute);
                        })
                ]))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
