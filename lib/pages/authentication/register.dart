// ignore_for_file: prefer_const_constructors, depend_on_referenced_packages

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:newpanale/constants/style.dart';
import 'package:newpanale/controllers/register_controller.dart';
import 'package:newpanale/routing/routes.dart';
import 'package:newpanale/widgets/custom_text.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:form_validator/form_validator.dart';

// ignore: must_be_immutable
class RegisterPage extends StatelessWidget {
  final registerController = Get.put(RegisterController());
  RegisterPage({super.key});

  GlobalKey<FormState> registerKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          constraints: BoxConstraints(maxWidth: 400),
          padding: EdgeInsets.all(24),
          child: Form(
            key: registerKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 12),
                      child: Image.asset("assets/icons/logo.png"),
                    ),
                    Expanded(child: Container()),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    Text("Register",
                        style: GoogleFonts.roboto(
                            fontSize: 30, fontWeight: FontWeight.bold)),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    CustomText(
                      text:
                          "Hello to all of our wonderful family and friends!.",
                      color: lightGrey,
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                TextFormField(
                  controller: registerController.emailController,
                  validator: ValidationBuilder().email().maxLength(50).build(),
                  decoration: InputDecoration(
                      labelText: "Email",
                      hintText: "abc@domain.com",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 15,
                ),
                TextFormField(
                  controller: registerController.nameController,
                  validator:
                      ValidationBuilder().minLength(2).maxLength(50).build(),
                  decoration: InputDecoration(
                      labelText: "Name",
                      hintText: "khali dhasan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 15,
                ),
                TextFormField(
                  obscureText: true,
                  controller: registerController.passwordController,
                  validator: ValidationBuilder()
                      .regExp(RegExp(r"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)"),
                          "Capital, small letter & Number & Special")
                      .minLength(6)
                      .maxLength(8)
                      .build(),
                  decoration: InputDecoration(
                      labelText: "Password",
                      hintText: "123",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 15,
                ),
                TextFormField(
                  obscureText: true,
                  controller: registerController.passwordconfirmationController,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter some text';
                    }
                    if (value != registerController.passwordController.text) {
                      return 'Password Confirmation Not Match';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      labelText: "Password Confirmation",
                      hintText: "123",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Checkbox(value: false, onChanged: (value) {}),
                        CustomText(
                          text: "Term & Conditions",
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                InkWell(
                  onTap: () async {
                    // Get.offAllNamed(rootRoute);
                    if (registerKey.currentState!.validate()) {
                      registerController.registerWithEmail();
                    }
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: active, borderRadius: BorderRadius.circular(20)),
                    alignment: Alignment.center,
                    width: double.maxFinite,
                    padding: EdgeInsets.symmetric(vertical: 16),
                    child: CustomText(
                      text: "Login",
                      color: Colors.white,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                RichText(
                  text: TextSpan(children: [
                    TextSpan(
                        text: "already have an account login.. ",
                        style: TextStyle(color: active),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            Get.offAllNamed(authenticationPageRoute);
                          }),
                  ]),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
