// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:newpanale/constants/controllers.dart';
import 'package:newpanale/helpers/reponsiveness.dart';
import 'package:newpanale/pages/overview/widgets/available_drivers_table.dart';
import 'package:newpanale/pages/overview/widgets/revenue_section_large.dart';
import 'package:newpanale/pages/overview/widgets/revenue_section_small.dart';
import 'package:newpanale/widgets/custom_text.dart';
import 'package:newpanale/pages/overview/widgets/overview_cards_large.dart';
import 'package:newpanale/pages/overview/widgets/overview_cards_small.dart';
import 'package:newpanale/pages/overview/widgets/overview_cards_medium.dart';

class OrverViewPage extends StatelessWidget {
  const OrverViewPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Obx(() => Row(
              children: [
                Container(
                  margin: EdgeInsets.only(
                      top: ResponsiveWidget.isSmallScreen(context) ? 56 : 6),
                  child: CustomText(
                    text: customMenuController.activeItem.value,
                    size: 24,
                    weight: FontWeight.bold,
                  ),
                )
              ],
            )),
        Expanded(
            child: ListView(
          children: [
            if (ResponsiveWidget.isLargeScreen(context) ||
                ResponsiveWidget.isMediumScreen(context))
              if (ResponsiveWidget.isCustomSize(context))
                OverviewCardsMediumScreen()
              else
                OverviewCardsLargeScreen()
            else
              OverviewCardsSmallScreen(),
            if (!ResponsiveWidget.isSmallScreen(context))
              RevenueSectionLarge()
            else
              RevenueSectionSmall(),
            AvailableDriversTable(),
          ],
        )),
      ],
    );
  }
}
