// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:newpanale/constants/style.dart';
import 'package:newpanale/widgets/custom_text.dart';

class InfoCardSmall extends StatelessWidget {
  final String title;
  final String value;
  final Color topColor;
  final bool isActive;
  final Function() onTap;

  const InfoCardSmall(
      {super.key,
      required this.title,
      required this.value,
      this.topColor = Colors.black12,
      required this.onTap,
      this.isActive = false});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.all(24),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: isActive ? active : lightGrey, width: .5),
          ),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            CustomText(
              text: title,
              size: 24,
              weight: FontWeight.w300,
              color: isActive ? active : lightGrey,
            ),
            CustomText(
              text: value,
              size: 24,
              weight: FontWeight.bold,
              color: isActive ? active : lightGrey,
            ),
          ]),
        ),
      ),
    );
  }
}
