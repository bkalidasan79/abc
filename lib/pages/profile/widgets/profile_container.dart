// ignore_for_file: unnecessary_import, prefer_const_constructors, use_key_in_widget_constructors, prefer_const_literals_to_create_immutables, must_be_immutable

import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:form_validator/form_validator.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:newpanale/constants/style.dart';
import 'package:newpanale/controllers/image_controller.dart';
import 'package:newpanale/controllers/profile_controller.dart';
import 'package:newpanale/widgets/custom_text.dart';

class ProfileContainer extends GetView<ProfileController> {
  final imageController = Get.put(ImageController());
  ProfileContainer({super.key});

  static GlobalKey<FormState> profileKye = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxWidth: 400),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: active.withOpacity(.4), width: .5),
        boxShadow: [
          BoxShadow(
              offset: Offset(0, 6),
              color: lightGrey.withOpacity(.1),
              blurRadius: 12)
        ],
        borderRadius: BorderRadius.circular(8),
      ),
      padding: EdgeInsets.all(16),
      margin: EdgeInsets.only(bottom: 30),
      child: Form(
        key: profileKye,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              children: [
                CustomText(
                  text: "Hello to all of our wonderful family and friends!.",
                  color: lightGrey,
                ),
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Obx(
              () => Row(
                children: [
                  InkWell(
                    onTap: () {
                      imageController.pickImage(ImageSource.gallery);
                    },
                    child: Container(
                        height: 250,
                        width: 250,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          image: DecorationImage(
                            image: NetworkImage(controller.imageurl.value),
                            fit: BoxFit.cover,
                          ),
                          border: Border.all(
                              color: active.withOpacity(.4), width: .5),
                          boxShadow: [
                            BoxShadow(
                                offset: Offset(0, 6),
                                color: lightGrey.withOpacity(.1),
                                blurRadius: 12)
                          ],
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: imageController.imagee.value == false
                            ? CustomText(
                                text: "Click to Image",
                                color: lightGrey,
                              )
                            : kIsWeb
                                ? Image.memory(imageController.webimage.value,
                                    fit: BoxFit.cover)
                                : Image.file(
                                    File(imageController.selectedfile.value),
                                    fit: BoxFit.cover)),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 15,
            ),
            TextFormField(
              controller: controller.emailController,
              validator: ValidationBuilder().email().maxLength(50).build(),
              enabled: false,
              decoration: InputDecoration(
                  labelText: "Email",
                  hintText: "abc@domain.com",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10))),
            ),
            SizedBox(
              height: 15,
            ),
            TextFormField(
              controller: controller.nameController,
              validator: ValidationBuilder().minLength(2).maxLength(50).build(),
              decoration: InputDecoration(
                  labelText: "Name",
                  hintText: "khali dhasan",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10))),
            ),
            SizedBox(
              height: 15,
            ),
            InkWell(
              onTap: () async {
                if (profileKye.currentState!.validate()) {
                  controller.saveProfile();
                  //Get.offAllNamed(profilePageRoute);
                }
              },
              child: Container(
                decoration: BoxDecoration(
                    color: active, borderRadius: BorderRadius.circular(20)),
                alignment: Alignment.center,
                width: double.maxFinite,
                padding: EdgeInsets.symmetric(vertical: 16),
                child: CustomText(
                  text: "Save",
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
