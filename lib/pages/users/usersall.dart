// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:newpanale/constants/controllers.dart';
import 'package:newpanale/controllers/users_controller.dart';
import 'package:newpanale/helpers/reponsiveness.dart';
import 'package:newpanale/pages/users/widgets/users_table.dart';

import 'package:newpanale/widgets/custom_text.dart';
import 'package:get/get.dart';

class UsersPage extends GetView<UsersController> {
  const UsersPage({super.key});

  @override
  Widget build(BuildContext context) {
    // ignore: avoid_unnecessary_containers
    return Container(
      child: Column(
        children: [
          Obx(
            () => Row(
              children: [
                Container(
                    margin: EdgeInsets.only(
                        top: ResponsiveWidget.isSmallScreen(context) ? 56 : 6),
                    child: CustomText(
                      text: customMenuController.activeItem.value,
                      size: 24,
                      weight: FontWeight.bold,
                    )),
              ],
            ),
          ),
          Flexible(
            child: UsersTable(),
          ),
        ],
      ),
    );
  }
}
