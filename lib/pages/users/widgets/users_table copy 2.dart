// ignore_for_file: unnecessary_import, prefer_const_constructors, prefer_const_literals_to_create_immutables, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:newpanale/constants/style.dart';
import 'package:newpanale/controllers/users_controller.dart';
import 'package:newpanale/models/users_mode.dart';

class UsersTable extends StatefulWidget {
  const UsersTable({
    Key? key,
  }) : super(key: key);

  @override
  State<UsersTable> createState() => UsersTablePage();
}

class UsersTablePage extends State<UsersTable> {
  bool sort = true;
  List<UsersModel>? filterData;

  final usersController = Get.put(UsersController());

/*   onsortColum(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        filterData!.sort((a, b) => a.name.compareTo(b.name));
      } else {
        filterData!.sort((a, b) => b.name.compareTo(a.name));
      }
    }
  } */

  @override
  void initState() {
    usersController.getUsers();
    // filterData = usersController.myUsersModel as List<UsersModel>?;
    super.initState();
  }

  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: active.withOpacity(.4), width: .5),
        boxShadow: [
          BoxShadow(
              offset: Offset(0, 6),
              color: lightGrey.withOpacity(.1),
              blurRadius: 12)
        ],
        borderRadius: BorderRadius.circular(8),
      ),
      padding: EdgeInsets.all(16),
      margin: EdgeInsets.only(bottom: 30, top: 20),
      child: ListView(children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: double.infinity,
              child: Obx(
                () => PaginatedDataTable(
                  sortColumnIndex: 1,
                  sortAscending: sort,
                  header: TextField(
                    controller: controller,
                    decoration: const InputDecoration(
                        hintText: "Enter something to filter"),
                    onChanged: (value) {
                      /* setState(() {
                      myUsersModel = filterData!
                          .where((element) => element.name.contains(value))
                          .toList();
                    }); */
                    },
                  ),
                  source: RowSource(
                    myData: usersController.myUsersModel,
                    count: usersController.myUsersModel.length,
                  ),
                  rowsPerPage: 10,
                  columnSpacing: 10,
                  columns: [
                    DataColumn(
                      label: const Text(
                        "Id",
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 14),
                      ),
                    ),
                    DataColumn(
                        label: Text(
                          "Name",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 14),
                        ),
                        onSort: (columnIndex, ascending) {
                          setState(() {
                            sort = !sort;
                          });

                          //onsortColum(columnIndex, ascending);
                        }),
                    DataColumn(
                      label: Text(
                        "email",
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 14),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        )
      ]),
    );
  }
}

class RowSource extends DataTableSource {
  var myData;
  final count;
  RowSource({
    required this.myData,
    required this.count,
  });

  @override
  DataRow? getRow(int index) {
    if (index < rowCount) {
      return recentFileDataRow(myData![index]);
    } else {
      return null;
    }
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => count;

  @override
  int get selectedRowCount => 0;
}

DataRow recentFileDataRow(var data) {
  return DataRow(
    cells: [
      DataCell(Text(data.id.toString())),
      DataCell(Text(data.name ?? "Name")),
      DataCell(Text(data.email.toString())),
    ],
  );
}
