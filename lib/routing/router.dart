// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:newpanale/pages/clients/clients.dart';
import 'package:newpanale/pages/drivers/drivers.dart';
import 'package:newpanale/pages/overview/overview.dart';
import 'package:newpanale/pages/authentication/authentication.dart';
import 'package:newpanale/pages/profile/profile.dart';
import 'package:newpanale/pages/users/usersall.dart';
import 'package:newpanale/routing/routes.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case overViewPageRoute:
      return _getPageRoute(OrverViewPage());
    case driversPageRoute:
      return _getPageRoute(DriversPage());
    case clientsPageRoute:
      return _getPageRoute(ClientsPage());
    case profilePageRoute:
      return _getPageRoute(ProfilePage());
    case userallPageRoute:
      return _getPageRoute(UsersPage());

    default:
      return _getPageRoute(AuthenticationPage());
  }
}

PageRoute _getPageRoute(Widget child) {
  return MaterialPageRoute(builder: (context) => child);
}
