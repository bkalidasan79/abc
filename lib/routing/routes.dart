// ignore_for_file: constant_identifier_names

const rootRoute = "/";

const overviewPageDisplayName = "Overview";
const overViewPageRoute = "/overview";

const driversPageDisplayName = "Drivers";
const driversPageRoute = "/drivers";

const clientsPageDisplayName = "Clients";
const clientsPageRoute = "/clients";

const authenticationPageDisplayName = "Log out";
const authenticationPageRoute = "/auth";

const registerPageDisplayName = "Register";
const registerPageRoute = "/register";

const forgotpasswordPageDisplayName = "Forgotpassword";
const forgotpasswordPageRoute = "/forgotpassword";

const profilePageDisplayName = "Profile";
const profilePageRoute = "/profile";

const userallPageDisplayName = "User List";
const userallPageRoute = "/usersall";

/* class MenuItem {
  final String name;
  final String route;

  MenuItem(this.name, this.route);
}

List<MenuItem> sideMenuItemRoutes = [
  MenuItem(overviewPageDisplayName, overViewPageRoute),
  MenuItem(driversPageDisplayName, driversPageRoute),
  MenuItem(clientsPageDisplayName, clientsPageRoute),
  MenuItem(profilePageDisplayName, profilePageRoute),
];
 */