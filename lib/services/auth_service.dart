import 'package:get/get.dart';

class AuthService extends GetxService {
  static AuthService get to => Get.find();

  /// Mocks a login process
  final isLoggedIn = false.obs;
  final username = ''.obs;
  final userprofileimage = ''.obs;
  bool get isLoggedInValue => isLoggedIn.value;
  //String get usernameValue => isLoggedIn.value;

  void login() {
    isLoggedIn.value = true;
  }

  void logout() {
    isLoggedIn.value = false;
  }

  void setusername(String user) {
    username.value = user;
  }
}
