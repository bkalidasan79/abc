import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:newpanale/models/users_mode.dart';
import 'package:newpanale/utils/api_endpoints.dart';

class UserServices {
  static var client = http.Client();

  static Future<List?> fetchUsers(String token) async {
    var headers = {
      'Content-Type': 'application/json',
      "Authorization": "Bearer $token"
    };

    var url = Uri.parse(
        "${ApiEndPoints.baseUrl}${ApiEndPoints.usersEndpoind.allUsers}");
    http.Response response = await http.get(url, headers: headers);

    if (response.statusCode == 200) {
      /*     var jsonString = response.body;
      return usersmodelFromJson(jsonString); */
      final json = await jsonDecode(response.body);
      var myUsersModel = [];
      for (var u in json) {
        UsersModel users = UsersModel.fromJson(u);
        myUsersModel.add(users);
      }
      return myUsersModel;
    } else {
      //show error message
      return null;
    }
  }
}
