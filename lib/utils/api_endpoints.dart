// ignore_for_file: prefer_const_declarations, library_private_types_in_public_api

class ApiEndPoints {
  static final String baseUrl = 'http://127.0.0.1:8000/api/';
  static _AuthEndPoints authEndpoints = _AuthEndPoints();
  static _Profile profileEndpoind = _Profile();
  static _Users usersEndpoind = _Users();
}

class _Profile {
  final String profileUpdate = 'profile/update';
  final String getProfile = 'profile/get';
}

class _AuthEndPoints {
  final String registerEmail = 'auth/register';
  final String loginEmail = 'auth/login';
  final String logoutEmail = 'auth/logout';
  final String me = 'auth/me';
}

class _Users {
  final String allUsers = 'users/all';
}
