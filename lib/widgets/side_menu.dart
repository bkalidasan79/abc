// ignore_for_file: prefer_const_constructors, avoid_print

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:newpanale/constants/controllers.dart';
import 'package:newpanale/constants/style.dart';
import 'package:newpanale/controllers/profile_controller.dart';

import '../routing/routes.dart';

final profileController = Get.put(ProfileController());

var myDrawer = Drawer(
  backgroundColor: light,
  elevation: 0,
  child: Column(
    children: [
      SizedBox(
        height: 50,
      ),
      Material(
          child: ListTile(
        leading: const Icon(Icons.dashboard),
        title: const Text('dashboard'),
        tileColor: light,
        hoverColor: hover,
        enabled: true,
        onTap: () {
          customMenuController.changeActiveitemTo('Dashboard');
          //  navigationController.navigateTo(overViewPageRoute);
          Get.offAllNamed(overViewPageRoute);
        },
      )),
      ExpansionTile(title: const Text("My Account"), children: [
        Material(
            child: ListTile(
          leading: const Icon(Icons.car_rental),
          title: const Text('Profile'),
          hoverColor: hover,
          enabled: true,
          onTap: () {
            customMenuController.changeActiveitemTo('Profile');
            navigationController.navigateTo(profilePageRoute);
          },
        )),
        Material(
            child: ListTile(
          leading: const Icon(Icons.flight),
          title: const Text('User'),
          hoverColor: hover,
          enabled: true,
          onTap: () {
            customMenuController.changeActiveitemTo('User');
            navigationController.navigateTo(userallPageRoute);
          },
        )),
        Material(
            child: ListTile(
          leading: const Icon(Icons.train),
          title: const Text('train'),
          hoverColor: hover,
          enabled: true,
          onTap: () {
            print('train');
          },
        )),
      ]),
      /* ExpansionTile(
          title: const Text("Profile"),          
          children: [
            Material(
                child: ListTile(
              leading: const Icon(Icons.car_rental),
              title: const Text('Car'),
              hoverColor: hover,
              enabled: true,
              onTap: () {
                print('Car');
              },
            )),
            Material(
                child: ListTile(
              leading: const Icon(Icons.flight),
              title: const Text('flight'),
              hoverColor: hover,
              enabled: true,
              onTap: () {
                print('flight');
              },
            )),
            Material(
                child: ListTile(
              leading: const Icon(Icons.train),
              title: const Text('train'),
              hoverColor: hover,
              enabled: true,
              onTap: () {
                print('train');
              },
            )),
          ]), */
    ],
  ),
);
