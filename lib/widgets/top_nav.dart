// ignore_for_file: prefer_const_constructors , prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:newpanale/constants/style.dart';
import 'package:newpanale/controllers/login_controller.dart';
import 'package:newpanale/helpers/reponsiveness.dart';
import 'package:newpanale/services/auth_service.dart';

final loginController = Get.put(LoginController());

AppBar topNavigationBar(BuildContext context, GlobalKey<ScaffoldState> key) =>
    AppBar(
      leading: !(ResponsiveWidget.isSmallScreen(context) ||
              ResponsiveWidget.isMediumScreen(context))
          ? Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 16),
                  child: Image.asset(
                    "assets/icons/logo.png",
                    width: 28,
                  ),
                ),
              ],
            )
          : IconButton(
              icon: const Icon(Icons.menu),
              onPressed: () {
                key.currentState?.openDrawer();
              }),
      title: Row(
        children: [
          Visibility(
            visible: (ResponsiveWidget.isSmallScreen(context) ||
                ResponsiveWidget.isMediumScreen(context)),
            child: Padding(
              padding: const EdgeInsets.only(left: 16),
              child: Image.asset(
                "assets/icons/logo.png",
                width: 28,
              ),
            ),
          ),
          Expanded(child: Container()),
          IconButton(
              icon: Icon(
                Icons.settings,
                color: dark,
              ),
              onPressed: () {}),
          Stack(
            children: [
              IconButton(
                  icon: Icon(
                    Icons.notifications,
                    color: dark.withOpacity(.7),
                  ),
                  onPressed: () {}),
              Positioned(
                top: 7,
                right: 7,
                child: Container(
                  width: 12,
                  height: 12,
                  padding: const EdgeInsets.all(4),
                  decoration: BoxDecoration(
                      color: active,
                      borderRadius: BorderRadius.circular(30),
                      border: Border.all(color: light, width: 2)),
                ),
              )
            ],
          ),
          Container(
            width: 1,
            height: 22,
            color: lightGrey,
          ),
          const SizedBox(
            width: 12,
          ),
          Obx(() => Text(AuthService.to.username.toString(),
              style: TextStyle(color: lightGrey))),
          const SizedBox(
            width: 16,
          ),
          IconButton(
              icon: Icon(
                Icons.logout,
                color: dark.withOpacity(.7),
              ),
              onPressed: () {
                loginController.logoutwithemail();
              }),
        ],
      ),
      iconTheme: IconThemeData(color: dark),
      elevation: 0,
      backgroundColor: Colors.transparent,
    );
